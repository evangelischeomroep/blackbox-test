let fs = require('fs');

//open and parse the file
try {
  let data = JSON.parse(fs.readFileSync('./config.json'))
  console.log(`the value of 'key' is ${data.key}, the value of 'secret' is ${data.secret}`)
} catch (e){
  console.log('We\'re catching an error, probably some parse thingy because you haven\'t decrypted the file yet');
  console.error(e);
}
